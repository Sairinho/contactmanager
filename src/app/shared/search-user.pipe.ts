import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "searchUser",
})
export class SearchUserPipe implements PipeTransform {
  /**
   * Pipe filters the list of elements based on the search text provided
   *
   * @param items list of elements to search in
   * @param searchText search string
   * @returns list of elements filtered by search text or []
   */
  transform(items: any[], searchText): any[] {
    let filteredList = [];
    if (searchText) {
      let newsearchText = !isNaN(searchText)
        ? searchText.toString()
        : searchText.toString().toUpperCase();
      let prop;
      return items.filter((item) => {
        for (let key in item) {
          prop = isNaN(item[key])
            ? item[key].toString().toUpperCase()
            : item[key].toString();
          if (prop.indexOf(newsearchText) > -1) {
            filteredList.push(item);
            return filteredList;
          }
        }
      });
    } else {
      return items;
    }
  }
}
