import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ContactService } from 'src/app/services/contact.service';
import { IContact } from '../../models/icontact';

@Component({
  selector: 'add-contact',
  templateUrl: './add-contact.component.html',
  styleUrls: ['./add-contact.component.scss']
})
export class AddContactComponent implements OnInit {
  public loading: boolean = false
  public contact: IContact = {} as IContact;
  public errorMessage: string | null = null;

  constructor(private _contactService: ContactService, private _router: Router) { }

  ngOnInit() {
  }

  /**
   * createSubmit
   *
   * Create a user when submited
   */
  public createSubmit() {
    this._contactService.createContact(this.contact).subscribe(data => {
      this._router.navigate(['/']).then()
    },(error) => {
      this.errorMessage = error;
      this._router.navigate(['/contacts/add']).then()
    })
  }

}
