import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ContactService } from '../../services/contact.service';

@Component({
  selector: 'view-contact',
  templateUrl: './view-contact.component.html',
  styleUrls: ['./view-contact.component.scss']
})
export class ViewContactComponent implements OnInit {
  public contactId: string | null = null;
  public contact;
  public loading = false;
  public errorMessage: string | null = null;

  constructor(private _activatedRoute: ActivatedRoute, private _contactService: ContactService) { }

  ngOnInit() {
    this._activatedRoute.paramMap.subscribe(param => {
      this.contactId = param.get('contactId')
    })
    this.loading = true;
    this._contactService.getContact(this.contactId).subscribe((data) => {
      this.contact = data
      this.loading = false;
    }, (error) => {
      this.errorMessage = error;
      this.loading = false;
    })
  }

  /**
   * isNotEmpty
   * check if received object is not empty
   */
  public isNotEmpty() {
    return Object.keys(this.contact).length > 0
  }
}
