import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IContact } from 'src/app/models/icontact';
import { ContactService } from 'src/app/services/contact.service';

@Component({
  selector: 'edit-contact',
  templateUrl: './edit-contact.component.html',
  styleUrls: ['./edit-contact.component.scss']
})
export class EditContactComponent implements OnInit {
  public loading: boolean = false
  public contactId: string | null = null;

  public contact: IContact = {} as IContact;
  public errorMessage: string | null = null;
  constructor(private _contactService: ContactService, private _router: Router,private _activatedRoute: ActivatedRoute ) { }

  ngOnInit() {
    this.loading = true;
    this._activatedRoute.paramMap.subscribe(param => {
      this.contactId = param.get('contactId')
    })
    if(this.contactId){
      this._contactService.getContact(this.contactId).subscribe(data => {
        this.contact = data;
        this.loading = false;
      },(error) => {
        this.errorMessage = error;
      })
    }
  }

  submitUpdate() {
    if(this.contactId){
      this._contactService.updateContact(this.contact, this.contactId).subscribe(data => {
        this._router.navigate(['/']).then();
      },(error) => {
        this.errorMessage = error;
        this._router.navigate([`/contacts/edit/${this.contactId}`]).then()
      })
    }
  }
}
