import { Component, OnInit } from '@angular/core';
import { IContact } from 'src/app/models/icontact';
import { ContactService } from 'src/app/services/contact.service';

@Component({
  selector: 'contact-manager',
  templateUrl: './contact-manager.component.html',
  styleUrls: ['./contact-manager.component.scss']
})
export class ContactManagerComponent implements OnInit {
  public loading: boolean = false
  public contacts: IContact[] = []
  public errorMessage: string | null = null;
  searchText = '';
  constructor(private _contactService: ContactService) { }

  ngOnInit() {
    this.getAllContacts()
  }

  /**
   * getAllContacts
   */
  public getAllContacts() {
    this.loading = true;
    this._contactService.getAllContacts().subscribe((data) => {
      this.contacts = data
      this.loading = false;
    }, (error) => {
      this.errorMessage = error;
      this.loading = false;
    })
  }
  /**
   * deleteContact
   * @contactId
   */
  public deleteContact(contactId) {
    if(contactId){
      this._contactService.deleteContact(contactId).subscribe(data => {
        this.getAllContacts()
      })
    }
  }
}
