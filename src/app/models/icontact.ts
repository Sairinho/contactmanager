export interface IContact {
  id?: string;
  name: string;
  email: string;
  photo: string;
  company: string;
  title: string;
  groupId: string;
  credits?: number;
}
