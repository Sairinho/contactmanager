import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { IContact } from '../models/icontact';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ContactService {
  private  serverURL: string = "http://localhost:3000"
  constructor(private _httpClient: HttpClient) { }

  public getAllContacts(): Observable<IContact[]> {
    let dataUrl = `${this.serverURL}/contacts`
    return this._httpClient.get<IContact[]>(dataUrl).pipe(catchError(this.handleError))
  }

  public getContact(contactId: string): Observable<IContact> {
    let dataUrl = `${this.serverURL}/contacts/${contactId}`
    return this._httpClient.get<IContact>(dataUrl).pipe(catchError(this.handleError))
  }


  public createContact(contact: IContact): Observable<IContact> {
    let dataUrl = `${this.serverURL}/contacts`
    return this._httpClient.post<IContact>(dataUrl, contact).pipe(catchError(this.handleError))
  }

  public updateContact(contact: IContact,contactId): Observable<IContact> {
    let dataUrl = `${this.serverURL}/contacts/${contactId}`
    return this._httpClient.put<IContact>(dataUrl, contact).pipe(catchError(this.handleError))
  }

  public deleteContact(contactId): Observable<{}> {
    let dataUrl = `${this.serverURL}/contacts/${contactId}`
    return this._httpClient.delete<{}>(dataUrl).pipe(catchError(this.handleError))
  }

  public handleError(error: HttpErrorResponse){
    let errorMessage: string = ''
    if(error.error instanceof ErrorEvent){
      // in case of client error
      errorMessage = `Error: ${error.error.message}`
    }
    else {
      // in case of server error
      errorMessage = `Status: ${error.status} \n Message: ${error.message}`

    }

    return throwError(errorMessage);
  }
}
